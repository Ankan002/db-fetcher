package main

import (
	"os"

	"github.com/ankan002/db-fetcher-trial/config"
	"github.com/ankan002/db-fetcher-trial/routes"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {
	config.LoadEnv()

	app := fiber.New()
	PORT := os.Getenv("PORT")

	app.Use(logger.New())

	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.Status(200).JSON(fiber.Map{
			"success": true,
			"message": "Hello from DB Fetcher API!!",
			"code":    200,
		})
	})

	router := app.Group("/api")

	routes.CollectionRouter(router)

	app.Listen(":" + PORT)
}
