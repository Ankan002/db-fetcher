package routes

import (
	"github.com/ankan002/db-fetcher-trial/controllers/collections"
	"github.com/gofiber/fiber/v2"
)

func CollectionRouter(router fiber.Router) {
	router.Post("/collection", collections.FetchCollectionController)
}
