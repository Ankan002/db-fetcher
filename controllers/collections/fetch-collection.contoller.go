package collections

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type RequestBody struct {
	DbUrl  string `json:"db_url"`
	DbName string `json:"db_name"`
}

func FetchCollectionController(ctx *fiber.Ctx) error {
	requestBody := RequestBody{}

	if err := ctx.BodyParser(&requestBody); err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"success": false,
			"error":   err.Error(),
			"code":    400,
		})
	}

	// dbContext, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	// defer cancel()

	dbClient, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(requestBody.DbUrl))

	if err != nil {
		return ctx.Status(400).JSON(fiber.Map{
			"success": false,
			"error":   err.Error(),
			"code":    400,
		})
	}

	foundCollections, _ := dbClient.Database(requestBody.DbName).ListCollectionNames(context.TODO(), bson.D{})

	for _, collection := range foundCollections {
		fmt.Println(collection)

		indexViewOfCollection := dbClient.Database(requestBody.DbName).Collection(collection).Indexes()

		indexList, err := indexViewOfCollection.List(context.TODO())

		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		var results []bson.M

		if err := indexList.All(context.TODO(), &results); err != nil {
			fmt.Println(err.Error())
			continue
		}

		for _, result := range results {
			for key, value := range result {
				fmt.Println(key, ":", value)
			}

			fmt.Println()
		}

		fmt.Println()
	}

	defer dbClient.Disconnect(context.TODO())

	return ctx.Status(200).JSON(fiber.Map{
		"success": true,
		"code":    200,
		"data": fiber.Map{
			"found_collections": foundCollections,
		},
	})
}
